import './page.css';
import Header from './sections/header/header';
import Banner from './sections/banner/banner';
import Build from './sections/build/build';
import Trend from './sections/trend/trend';
import Design from './sections/design/design';
import Footer from './sections/footer/footer';

function Page() {
    return (
        <>
            <Header/>
            <Banner/>
            <Build/>
            <Trend/>
            <Design/>
            <Footer/>
        </>
    );
}

export default Page;
