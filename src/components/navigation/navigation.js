import './navigation.css';
import Link from '../link/link';

function Navigation() {
    return (
        <nav className='header__nav'>
            <ul>
                <li>
                    <Link linkUrl={'/'} linkValue={'Home'}/>
                </li>
                <li>
                    <Link linkUrl={'/#contact'} linkValue={'Contact'}/>
                </li>
                <li>
                    <Link linkUrl={'/#affiliate'} linkValue={'Affiliate'}/>
                </li>
                <li>
                    <Link linkUrl={'/#about'} linkValue={'About'}/>
                </li>
                <li>
                    <Link linkUrl={'/#faq'} linkValue={'FAQ'}/>
                </li>
            </ul>
            <div className="header__burger">
                <span></span>
            </div>
        </nav>
    );
}

export default Navigation;
