import './abilities-item.css';
import cusIcon from '../../img/icons/ic_customize.svg';
import disIcon from '../../img/icons/ic_discover.svg';
import optIcon from '../../img/icons/ic_optimize.svg';

function AbilitiesItem({abilitiesIcon, abilitiesTitle}) {

    let abilitiesImg =
        abilitiesIcon === 'cus' ? cusIcon :
            abilitiesIcon === 'dis' ? disIcon :
                abilitiesIcon === 'opt' ? optIcon :
                    disIcon
    ;

    let abilitiesImgAlt =
        abilitiesIcon === 'cus' ? 'Customize' :
            abilitiesIcon === 'dis' ? 'Discover' :
                abilitiesIcon === 'opt' ? 'Thunder' :
                    'Thunder'
    ;

    return (
        <div className='abilities__item'>
            <div>
                <img src={abilitiesImg} alt={abilitiesImgAlt}/>
            </div>
            <span>{abilitiesTitle}</span>
        </div>
    );
}

export default AbilitiesItem;
