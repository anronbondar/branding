import './link.css';

function Link({linkUrl, linkValue}) {
    return (
        <a className='link' href={linkUrl}>{linkValue}</a>
    );
}

export default Link;
