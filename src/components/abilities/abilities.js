import './abilities.css';
import AbilitiesItem from '../abilities-item/abilities-item';

function Abilities() {
    return (
        <div className='abilities'>
            <AbilitiesItem abilitiesIcon={'cus'} abilitiesTitle={'Customize'}/>
            <AbilitiesItem abilitiesIcon={'dis'} abilitiesTitle={'Discover'}/>
            <AbilitiesItem abilitiesIcon={'opt'} abilitiesTitle={'Optimize'}/>
        </div>
    );
}

export default Abilities;
