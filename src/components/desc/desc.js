import './desc.css';

function Desc({descClass, descValue}) {
    return (
        <div className={`${descClass} description`}>
            {descValue}
        </div>
    );
}

export default Desc;
