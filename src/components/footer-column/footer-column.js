import {useState, useEffect} from 'react';
import './footer-column.css';

function FooterColumn({columnTitle, columnList}) {
    const [list, setList] = useState([]);

    useEffect(() => {
        setList(columnList);
    }, []);

    return (
        <div className='footer__column'>
            <span>{columnTitle}</span>
            <ul>
                {list.map((e) => {
                    return <li key={e}>{e}</li>
                })}
            </ul>
        </div>
    );

}

export default FooterColumn;
