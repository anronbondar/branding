import './title.css';

function Title({titleValue}) {
  return (
      <div className='title'>
        {titleValue}
      </div>
  );
}

export default Title;
