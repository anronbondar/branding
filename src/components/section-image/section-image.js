import './section-image.css';
import buildImg from '../../img/consistency_img.png';
import trendImg from '../../img/trendbird_img.png';
import designImg from '../../img/design_img.png';

function SectionImage({whatSection}) {

    let secImg =
        whatSection === 'build' ? buildImg :
            whatSection === 'trend' ? trendImg :
                whatSection === 'design' ? designImg :
                    trendImg
    ;

    let secImgAlt =
        whatSection === 'build' ? 'build' :
            whatSection === 'trend' ? 'trend' :
                whatSection === 'design' ? 'design' :
                    trendImg
    ;

    return (
        <div className='sec__image'>
            <img src={secImg} alt={secImgAlt}/>
        </div>
    );
}

export default SectionImage;
