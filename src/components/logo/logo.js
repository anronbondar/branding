import Link from '../link/link';
import logo from '../../img/icons/logo.svg';

function Logo() {
    return (
        <Link
            linkUrl={'/'}
            linkValue={<img src={logo} alt='logo'/>}
        />
    );
}

export default Logo;
