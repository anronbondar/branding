import './button.css';

function Button({btnValue}) {
    return (
        <button className='button'>{btnValue}</button>
    );
}

export default Button;
