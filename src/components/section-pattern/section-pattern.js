import './section-pattern.css';
import SectionImage from '../section-image/section-image';
import SectionText from '../section-text/section-text';

function SectionPattern({whatSection}) {
    return (
        <section>
            <div className='container'>
                <div className={`sec ${whatSection === 'trend' ? 'sec_reverse' : ''}`}>
                    <SectionImage whatSection={whatSection}/>
                    <SectionText whatSection={whatSection}/>
                </div>
            </div>
        </section>
    );
}

export default SectionPattern;
