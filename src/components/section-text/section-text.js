import './section-text.css';
import Title from '../title/title';
import Desc from '../desc/desc';
import Button from '../button/button';

function SectionText({whatSection}) {

    let secTitle =
        whatSection === 'build' ? 'Build Consistency' :
            whatSection === 'trend' ? 'Be An Early Design Bird' :
                whatSection === 'design' ? 'Design An Eye-Catching Logo' :
                    'Title'
    ;

    let secDesc =
        whatSection === 'build' ? 'Create a right feel for your brand. Our AI Solution can generate up to 1000 unique fonts that convey just the right message and emotion for your brand.' :
            whatSection === 'trend' ? 'Leverage our trend analysis algorithm. Become a trend setter in your niche and improve your lead generation.' :
                whatSection === 'design' ?
                    <>Be creative. Search for an unknown and unexplored.<br/> Design a memorable and eye-catching logo
                        that will foster brand loyalty.<br/>
                        <div>With our AI algoritm you can:
                            <ul>
                                <li>Personalize brand identity</li>
                                <li>Make a right impression</li>
                                <li>Generate up to 1000 unique logos</li>
                                <li>Alongside a right slogan</li>
                            </ul>
                        </div>
                    </> :
                    'Description'
    ;

    let secButton =
        whatSection === 'build' ? 'Contact Us' :
            whatSection === 'trend' ? 'Generate' :
                whatSection === 'design' ? 'Contact Us' :
                    'Submit'
    ;

    return (
        <div className='sec__text'>
            <Title titleValue={secTitle}/>
            <Desc descClass={'sec__desc'} descValue={secDesc}/>
            <Button btnValue={secButton}/>
        </div>
    );
}

export default SectionText;
