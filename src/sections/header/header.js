import './header.css';
import Logo from '../../components/logo/logo';
import Navigation from '../../components/navigation/navigation';

function Header() {
    return (
        <header className='header'>
            <div className='container'>
                <div className='header__row'>
                    <Logo/>
                    <Navigation/>
                </div>
            </div>
        </header>
    );
}

export default Header;
