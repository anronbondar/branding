import './footer.css';
import FooterColumn from '../../components/footer-column/footer-column';

function Footer() {

    let customerArr = ['Contact Us', 'Canceling of Subscription', 'Follow Us', 'Help & FAQ'];
    let learnArr = ['About Us', 'Careers', 'Affiliate'];
    let legalArr = ['Privacy Policy', 'Terms of Service', 'Cookie Policy'];

    return (
        <header className='footer'>
            <div className='container'>
                <div className='footer__row'>
                    <div className='footer__columns'>
                        <FooterColumn columnTitle={'Customer Care'} columnList={customerArr}/>
                        <FooterColumn columnTitle={'Learn More'} columnList={learnArr}/>
                        <FooterColumn columnTitle={'Legal Info'} columnList={legalArr}/>
                    </div>
                    <div className='footer__rights'>
                        Follow Us <span>2022</span> All rights reserved
                    </div>
                </div>
            </div>
        </header>
    );
}

export default Footer;
