import './banner.css';
import Button from "../../components/button/button";
import Desc from '../../components/desc/desc';

function Banner() {
    return (
        <div className='banner'>
            <div className='container'>
                <div className='banner__row'>
                    <h1>Deconstruct Branding</h1>
                    <Desc
                        descClass={'banner__desc'}
                        descValue={'Evaluate your brand competitiveness and explore key improvement opportunities'}
                    />
                    <Button btnValue={'Contact Us'}/>
                </div>
            </div>
        </div>
    );
}

export default Banner;
