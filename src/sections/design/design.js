import './design.css';
import SectionPattern from '../../components/section-pattern/section-pattern';

function Design() {
    return (
        <div className='design'>
            <SectionPattern whatSection={'design'}/>
        </div>
    );
}

export default Design;
