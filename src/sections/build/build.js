import Abilities from '../../components/abilities/abilities';
import SectionPattern from '../../components/section-pattern/section-pattern';

function Build() {
    return (
        <div>
            <Abilities/>
            <SectionPattern whatSection={'build'}/>
        </div>
    );
}

export default Build;
