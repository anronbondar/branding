import './trend.css';
import SectionPattern from '../../components/section-pattern/section-pattern';

function Trend() {
    return (
        <div className='trend'>
            <SectionPattern whatSection={'trend'}/>
        </div>
    );
}

export default Trend;
